<!DOCTYPE html>
<html>
<head>
  <title>Nick D</title>

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <link href="https://fonts.googleapis.com/css?family=Libre+Franklin|Lora:400,700" rel="stylesheet" />
  <link href="/resources/css/styles.css" media="all" rel="stylesheet" />

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-2626422-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-2626422-1');
  </script>
</head>

<body>

  <header>
    <h1>Nick D</h1>
  </header>

  <section>
    <a href="mailto:nicholas.dykzeul@gmail.com" title="It's my email address. Yup, really.">Nicholas.Dykzeul@Gmail.com</a> |
    <a href="https://www.linkedin.com/in/nicholasdykzeul/" title="View my LinkedIn profile.">LinkedIn</a>
  </section>

</body>
</html>
